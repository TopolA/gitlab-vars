variable "token" {
    description = "API Token for GitLab access"
}

variable "id" {
   description = "Your project ID or PATH (like TopolA/test)"
}

