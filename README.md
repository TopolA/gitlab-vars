Get gitlab project params

```
variable "gitlab_project_id" { description = "Your GitLab project IP or path" }

module "runner" {
   source = "git::https://gitlab.com/TopolA/gitlab-vars.git"
   
   id     = var.gitlab_project_id
   token  = var.gitlab_token
}

# User outputs:
# module.runner.runners_token
```

Include this snippet at your terraform file, define two variables and user outputs

