terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      # version = "3.6.0"
    }
  }
}

provider "gitlab" { token = var.token }

data "gitlab_project" "topola" {
   id = var.id
}

